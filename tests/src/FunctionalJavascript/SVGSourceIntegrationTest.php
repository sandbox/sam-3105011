<?php

namespace Drupal\Tests\media_entity_svg\FunctionalJavascript;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Tests\media\FunctionalJavascript\MediaSourceTestBase;
use Drupal\Tests\media_entity_svg\Unit\SVGFixtureTrait;

/**
 * Test the source plugin.
 *
 * @group media_entity_svg
 */
class SVGSourceIntegrationTest extends MediaSourceTestBase {

  use SVGFixtureTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'media_entity_svg',
  ];

  /**
   * Test the SVG source.
   */
  public function testSvgMediaType() {
    $media_type = $this->createMediaType('svg');
    $display = EntityViewDisplay::create([
      'targetEntityType' => 'media',
      'bundle' => $media_type->id(),
      'mode' => 'default',
      'status' => TRUE,
    ]);
    $media_type->getSource()->prepareViewDisplay($media_type, $display);
    $display->save();

    $this->drupalGet('media/add/' . $media_type->id());
    $this->getSession()->getPage()->attachFileToField('files[field_media_svg_0]', $this->pathToFixture('valid.svg'));
    $this->assertSession()->waitForButton('Remove');
    $this->submitForm([
      'name[0][value]' => 'Sample',
    ], 'Save');

    $this->drupalGet('media/1');

    $this->assertSession()->responseContains('<svg viewBox="0 0 24 24">');
    $this->assertSession()->responseContains('valid.svg#Layer_1');
  }

}
