<?php

namespace Drupal\Tests\media_entity_svg\Kernel;

use Drupal\file\Entity\File;
use Drupal\KernelTests\KernelTestBase;
use Drupal\media_entity_svg\Plugin\Validation\Constraint\SVGMediaConstraint;
use Drupal\media_entity_svg\Plugin\Validation\Constraint\SVGMediaConstraintValidator;
use Drupal\Tests\media_entity_svg\Unit\SVGFixtureTrait;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @coversDefaultClass \Drupal\media_entity_svg\Plugin\Validation\Constraint\SVGMediaConstraintValidator
 */
class SVGMediaConstraintValidatorTest extends KernelTestBase {

  use SVGFixtureTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'user',
    'file',
  ];

  /**
   * @covers ::validate
   * @dataProvider svgValidationTestCases
   */
  public function testSvgValidation($xmlFile, $expectedMessages) {
    $context = $this->prophesize(ExecutionContextInterface::class);
    if ($expectedMessages) {
      $context->addViolation($expectedMessages)->shouldBeCalled();
    }
    else {
      $context->addViolation()->shouldNotBeCalled();
    }

    $validator = new SVGMediaConstraintValidator();
    $validator->initialize($context->reveal());

    $validator->validate((object) [
      'entity' => File::create([
        'uri' => $this->pathToFixture($xmlFile),
      ]),
    ], new SVGMediaConstraint());
  }

  /**
   * Test cases for ::testSVGValidation.
   */
  public function svgValidationTestCases() {
    return [
      'Valid' => [
        'valid.svg',
        NULL,
      ],
      'No ID' => [
        'no-id.svg',
        'No valid ID was found on the root of the SVG.',
      ],
      'No viewbox' => [
        'no-viewbox.svg',
        'No valid viewBox attribute was found on the root of the SVG.',
      ],
      'Invalid' => [
        'invalid.svg',
        'An invalid SVG file was uploaded.',
      ],
    ];
  }

}
