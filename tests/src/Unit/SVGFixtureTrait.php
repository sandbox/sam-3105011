<?php

namespace Drupal\Tests\media_entity_svg\Unit;

/**
 * Fixture test trait.
 */
trait SVGFixtureTrait {

  /**
   * Get the path to a fixture.
   *
   * @param string $fixtureName
   *   The fixture name.
   *
   * @return string
   *   The path to the fixture.
   */
  protected function pathToFixture($fixtureName) {
    return __DIR__ . '/../../fixtures/' . $fixtureName;
  }

  /**
   * Get a fixture.
   *
   * @param string $fixtureName
   *   The fixture name.
   *
   * @return string
   *   The contents of the fixture.
   */
  protected function getFixture($fixtureName) {
    return file_get_contents($this->pathToFixture($fixtureName));
  }

}
