<?php

namespace Drupal\Tests\media_entity_svg\Unit;

use Drupal\media_entity_svg\SVG;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\media_entity_svg\SVG
 */
class SVGTest extends UnitTestCase {

  use SVGFixtureTrait;

  /**
   * Test the SVG class.
   */
  public function testSvg() {
    $svg = new SVG($this->getFixture('valid.svg'));
    $this->assertEquals('0 0 24 24', $svg->getViewBox());
    $this->assertEquals('Layer_1', $svg->getId());
  }

}
