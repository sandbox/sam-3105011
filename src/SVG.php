<?php

namespace Drupal\media_entity_svg;

use Drupal\file\FileInterface;

/**
 * An SVG utility.
 *
 * @method int getId();
 * @method int getHeight();
 * @method int getWidth();
 * @method string getViewBox();
 * @method string getFill();
 */
class SVG {

  /**
   * The attributes.
   *
   * @var array
   */
  protected $attributes;

  /**
   * The title.
   *
   * @var string
   */
  protected $title;

  /**
   * Create an instance of SVG.
   */
  public function __construct($domElement) {
    $domElement = $this->loadXml($domElement);

    $attributes = $domElement->attributes();
    foreach ($attributes as $key => $value) {
      $this->attributes[$key] = (string) $value;
    }

    // Keep the XML around as well.
    $this->xmlString = $domElement->asXML();

    // Handle the title element.
    $this->title = !empty($domElement->title) ? (string) $domElement->title : '';
  }

  /**
   * Create an instance of the SVG wrapper from a file entity.
   *
   * @return static
   *   An SVG instance for a given file.
   */
  public static function createFromFile(FileInterface $file) {
    $contents = file_get_contents($file->getFileUri());
    return new static($contents);
  }

  /**
   * Get the title.
   *
   * @return string
   *   The title of the SVG.
   */
  public function getTitle() {
    return trim($this->title);
  }

  /**
   * Dynamically implement our attribute getters.
   *
   * @param string $name
   *   The method name.
   * @param array $arguments
   *   An array of arguments.
   *
   * @return string
   *   The method result.
   */
  public function __call($name, array $arguments) {
    if (substr($name, 0, 3) === 'get') {
      $key = lcfirst(substr($name, 3));
      return $this->getAttribute($key);
    }
    throw new \BadMethodCallException();
  }

  /**
   * Get an attribute.
   *
   * @param string $key
   *   The attribute name.
   *
   * @return string|null
   *   The attribute value.
   */
  public function getAttribute($key) {
    return !empty($this->attributes[$key]) ? $this->attributes[$key] : NULL;
  }

  /**
   * Get the original XML.
   *
   * @return string
   *   The XML.
   */
  public function getXml() {
    return $this->xmlString;
  }

  /**
   * Load an XML string.
   */
  protected function loadXml($domElement) {
    if (is_string($domElement)) {
      $domElement = @simplexml_load_string($domElement);
    }
    if (!$domElement instanceof \SimpleXMLElement) {
      throw new \InvalidArgumentException('The string or element is not a valid SVG file');
    }
    return $domElement;
  }

}
