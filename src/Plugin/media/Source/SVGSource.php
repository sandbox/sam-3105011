<?php

namespace Drupal\media_entity_svg\Plugin\media\Source;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaSourceFieldConstraintsInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\media_entity_svg\SVG;

/**
 * SVG media source.
 *
 * @MediaSource(
 *   id = "svg",
 *   label = @Translation("SVG"),
 *   description = @Translation("Use SVG files as reusable media."),
 *   allowed_field_types = {"file"},
 *   default_thumbnail_filename = "generic.png",
 *   forms = {
 *     "media_library_add" = "\Drupal\media_entity_svg\Form\SvgUploadForm"
 *   }
 * )
 */
class SVGSource extends MediaSourceBase implements MediaSourceFieldConstraintsInterface {

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
      SVGSourceInterface::METADATA_ATTRIBUTE_ROOT_VIEWBOX => $this->t('Viewbox'),
      SVGSourceInterface::METADATA_ATTRIBUTE_ROOT_ID => $this->t('ID'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    $svg = SVG::createFromFile($this->getFileEntity($media));
    switch ($attribute_name) {
      case SVGSourceInterface::METADATA_ATTRIBUTE_ROOT_VIEWBOX:
        return $svg->getViewBox();

      case SVGSourceInterface::METADATA_ATTRIBUTE_ROOT_ID:
        return $svg->getId();

      default:
        return parent::getMetadata($media, $attribute_name);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFileEntity(MediaInterface $media) {
    $source_field = $this->configuration['source_field'];
    if (empty($source_field)) {
      throw new \RuntimeException('Source field for media source is not defined.');
    }
    /** @var \Drupal\Core\Field\FieldItemInterface $field_item */
    return $media->get($source_field)->first()->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceFieldConstraints() {
    return [
      'SVGMedia' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    return parent::createSourceField($type)->set('settings', ['file_extensions' => 'svg']);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareViewDisplay(MediaTypeInterface $type, EntityViewDisplayInterface $display) {
    $display->setComponent($this->getSourceFieldDefinition($type)->getName(), [
      'type' => 'media_entity_svg_file_formatter',
      'region' => 'content',
    ]);
  }

}
