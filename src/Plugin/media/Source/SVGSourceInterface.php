<?php

namespace Drupal\media_entity_svg\Plugin\media\Source;

use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceFieldConstraintsInterface;
use Drupal\media\MediaSourceInterface;

/**
 * Interface for a CSV source.
 */
interface SVGSourceInterface extends MediaSourceInterface, MediaSourceFieldConstraintsInterface {

  /**
   * Key for the viewbox attribute.
   *
   * @var string
   */
  const METADATA_ATTRIBUTE_ROOT_VIEWBOX = 'viewbox';

  /**
   * Key for the root ID attribute.
   *
   * @var string
   */
  const METADATA_ATTRIBUTE_ROOT_ID = 'id';

  /**
   * Get a file entity from a media entity of this type.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity.
   *
   * @return \Drupal\file\FileInterface
   *   The file.
   */
  public function getFileEntity(MediaInterface $media);

}
