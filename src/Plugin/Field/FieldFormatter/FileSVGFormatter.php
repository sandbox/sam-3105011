<?php

namespace Drupal\media_entity_svg\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Url;
use Drupal\media_entity_svg\SVG;

/**
 * Plugin implementation of the 'media_svg_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "media_entity_svg_file_formatter",
 *   label = @Translation("File SVG formatter"),
 *   field_types = {
 *      "file",
 *   }
 * )
 */
class FileSVGFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    /** @var \Drupal\media\MediaInterface $media */
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $svg = SVG::createFromFile($file);
      $viewBox = $svg->getViewBox();
      $id = $svg->getId();
      $elements[] = [
        '#theme' => 'media_svg',
        '#href' => (string) Url::fromUri($file->createFileUrl(FALSE))->toString(),
        '#id' => $id,
        '#viewbox' => $viewBox,
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getFieldStorageDefinition()->getTargetEntityTypeId() === 'media';
  }

}
