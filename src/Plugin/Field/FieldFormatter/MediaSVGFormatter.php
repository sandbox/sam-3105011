<?php

namespace Drupal\media_entity_svg\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Url;
use Drupal\media_entity_svg\Plugin\media\Source\SVGSourceInterface;

/**
 * Plugin implementation of the 'media_svg_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "media_entity_svg_media_formatter",
 *   label = @Translation("Media SVG formatter"),
 *   field_types = {
 *     "entity_reference",
 *   }
 * )
 */
class MediaSVGFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    /** @var \Drupal\media\MediaInterface $media */
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $media) {
      /** @var \Drupal\media_entity_svg\Plugin\media\Source\SVGSourceInterface $source */
      $source = $media->getSource();
      $file = $source->getFileEntity($media);
      $viewBox = $source->getMetadata($media, SVGSourceInterface::METADATA_ATTRIBUTE_ROOT_VIEWBOX);
      $id = $source->getMetadata($media, SVGSourceInterface::METADATA_ATTRIBUTE_ROOT_ID);
      $elements[] = [
        '#theme' => 'media_svg',
        '#href' => (string) Url::fromUri($file->createFileUrl(FALSE))
          ->toString(),
        '#id' => $id,
        '#viewbox' => $viewBox,
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $target = $field_definition->getFieldStorageDefinition()->getSetting('target_type');
    return $target === 'media';
  }

}
