<?php

namespace Drupal\media_entity_svg\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Check if a file is a valid SVG.
 *
 * @Constraint(
 *   id = "SVGMedia",
 *   label = @Translation("Validate the SVG media file.", context = "Validation"),
 *   type = {"file"}
 * )
 */
class SVGMediaConstraint extends Constraint {

  /**
   * Invalid SVG message.
   *
   * @var string
   */
  public $invalidSvg = 'An invalid SVG file was uploaded.';

  /**
   * Invalid view box message.
   *
   * @var string
   */
  public $invalidViewBox = 'No valid viewBox attribute was found on the root of the SVG.';

  /**
   * Invalid ID message.
   *
   * @var string
   */
  public $invalidId = 'No valid ID was found on the root of the SVG.';

}
