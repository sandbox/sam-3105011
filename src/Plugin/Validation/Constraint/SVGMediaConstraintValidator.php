<?php

namespace Drupal\media_entity_svg\Plugin\Validation\Constraint;

use Drupal\media_entity_svg\SVG;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the SVGMediaConstraint constraint.
 */
class SVGMediaConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    /** @var \Drupal\file\Plugin\Field\FieldType\FileFieldItemList $value */
    /** @var \Drupal\media_entity_svg\Plugin\Validation\Constraint\SVGMediaConstraint $constraint */
    /** @var \Drupal\file\FileInterface $file */
    if (!$file = $value->entity) {
      return;
    }

    try {
      $svg = SVG::createFromFile($file);
    }
    catch (\Exception $e) {
      $this->context->addViolation($constraint->invalidSvg);
      return;
    }
    if (!$svg->getViewBox()) {
      $this->context->addViolation($constraint->invalidViewBox);
    }
    if (!$svg->getId()) {
      $this->context->addViolation($constraint->invalidId);
    }
  }

}
