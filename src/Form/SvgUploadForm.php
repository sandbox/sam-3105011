<?php

namespace Drupal\media_entity_svg\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\media\MediaInterface;
use Drupal\media_library\Form\AddFormBase;
use Drupal\media_library\Form\FileUploadForm;

/**
 * A form to integrate the SVG uploads into the media library.
 */
class SvgUploadForm extends FileUploadForm {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityFormElement(MediaInterface $media, array $form, FormStateInterface $form_state, $delta) {
    // Skip the ::buildEntityFormElement of the file upload form, since it
    // hides files from entity displays. Since validation of the SVG is a
    // constraint, the file element must be present on the entity form to
    // validate the SVG is valid.
    return AddFormBase::buildEntityFormElement($media, $form, $form_state, $delta);
  }

}
